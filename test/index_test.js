const assert = require('chai').assert;
const lambdaLocal = require('lambda-local');
const handler = require('../src/index');

describe('handler', () => {
  console.log(handler);
  it('should return "Success"', () => {
    lambdaLocal.execute({
      event: {},
      lambdaFunc: handler,
      callback: (error, data) => {
        assert.equal(data, "Success");
      }
    })
  });
});