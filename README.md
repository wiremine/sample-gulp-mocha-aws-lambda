# Gulp Sample App

A simple gulp sample app.

## Getting Started

### Installation

- `npm install gulp-cli -g` - Install the gulp CLI
- `npm install` - Install the other deps

## Other Links

https://medium.com/@AdamRNeary/a-gulp-workflow-for-amazon-lambda-61c2afd723b6
