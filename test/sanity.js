const assert = require('assert');

describe('sanity', () => {
  it('should sum numbers correctly', () => {
    assert.equal(1 + 1, 2);
  });
});