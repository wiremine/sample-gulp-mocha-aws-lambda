const gulp = require('gulp');
const mocha = require('gulp-mocha');
const install = require('gulp-install');
const zip = require('gulp-zip');
const del = require("del");

const ARCHIVE_NAME = 'archive.zip';

// Run the mocha tests
gulp.task('test', () => {
  return gulp.src(['test/**/*.js'], {
      read: false
    })
    .pipe(mocha());
});

// Watch src/** and  test/** and automatically run the test task
gulp.task('watch', () => {
  gulp.watch(['src/**', 'test/**'], ['test']);
});

// Package Labmda and deps to dist/
gulp.task('js', () => {
  return gulp.src('src/**/*.js')
    .pipe(gulp.dest('./dist/'));
});

// Install the production NPM mods into dist/
gulp.task('npm', () => {
  return gulp.src('./package.json')
    .pipe(gulp.dest('./dist'))
    .pipe(install({
      production: true
    }));
});

// Zip the archive 
gulp.task('zip', () => {
  return gulp.src(['dist/**/*', '!dist/package.json', 'dist/.*'])
    .pipe(zip(ARCHIVE_NAME))
    .pipe(gulp.dest('./'));
});

gulp.task('clean', () => {
  return del(['dist/', ARCHIVE_NAME]);
});

gulp.task('build', gulp.series(['clean', 'test', 'npm', 'js', 'zip']));

gulp.task('default', gulp.series(['test']));